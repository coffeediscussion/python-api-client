from typing import Tuple
from urllib.parse import urlencode


def build_url(host: str, path: Tuple[str, ...] = (), query: dict = None, schema: str = 'https',
              port: int = None) -> str:
    if query is None:
        query = {}

    url = schema + '://' + host
    if port:
        url += ':' + str(port)
    url += '/' + '/'.join([part.strip('/') for part in path])
    if len(query) > 0:
        values = [
            (key, value if type(value) is tuple else (value,))
            for key, value in query.items()
        ]

        values = [
            (key, item)
            for key, value in values
            for item in value
        ]

        values = [urlencode({key: value}) for (key, value) in values]
        url += '?' + "&".join(values)
    return url
