from unittest import TestCase

from utils.url_utils import build_url


class TestBuildUrl(TestCase):

    def test_build_url(self):
        self.assertEqual('http://ssoa.sterbc.com:80/as/token.oauth2?date=2018-01-01',
                         build_url('ssoa.sterbc.com', ('as/token.oauth2',), {'date': '2018-01-01'}, 'http', 80))

    def test_build_url_no_query(self):
        self.assertEqual('https://ssoa.sterbc.com/as/token.oauth2',
                         build_url('ssoa.sterbc.com', ('as/token.oauth2',)))

    def test_build_url_extra_slash(self):
        self.assertEqual('https://ssoa.sterbc.com/as/token.oauth2',
                         build_url('ssoa.sterbc.com', ('/as/token.oauth2/',)))
