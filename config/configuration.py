class EndpointConfiguration(object):

    def __init__(self, host: str, root_path: str, scheme: str = 'https'):
        self.scheme = scheme
        self.rootPath = root_path
        self.host = host


class ApiEndpointConfiguration(EndpointConfiguration):
    pass


class AuthEndpointConfiguration(EndpointConfiguration):

    def __init__(self, host: str, root_path: str, client_id: str, client_secret: str, scheme: str = 'https'):
        super().__init__(host, root_path, scheme)
        self.clientSecret = client_secret
        self.clientId = client_id
