import getopt
import sys
from pathlib import Path
from typing import Any, Callable, Dict, List

import yaml


def get_profile(opts):
    try:
        return next(value for opt, value in opts if opt == '--profile')
    except StopIteration:
        return None


def load_yaml_file(filename: str, consumer: Callable[[dict], None]):
    path = Path(filename)
    if path.is_file():
        with open(filename) as f:
            val = yaml.safe_load(f)
            if val:
                consumer(val)


def merge_configuration(conf1: dict, conf2: dict):
    if (conf1 is None) or (conf2 is None):
        return
    for key, value in conf2.items():
        existing = conf1.get(key)
        if (type(existing) is dict) and (type(value) is dict):
            merge_configuration(existing, value)
        else:
            conf1[key] = value


def configuration_value(configuration: Dict[str, str], prop_name: str, prop_value: str):
    path = prop_name.split(".")
    set_configuration_value(configuration, path, prop_value)


def get_or_compute(dictionary: dict, key: str, producer: Callable[[str], Any]):
    val = dictionary.get(key)
    if val is None:
        val = producer(key)
        dictionary[key] = val
    return val


def set_configuration_value(configuration: dict, path: List[str], prop_value: str):
    if len(path) == 0:
        return
    if len(path) == 1:
        configuration[path[0]] = prop_value
        return
    sub_configuration = get_or_compute(configuration, path[0], lambda key: {})
    set_configuration_value(sub_configuration, path[1:], prop_value)


def load_configuration(properties: List[str] = None):
    if properties is None:
        properties = []
    properties = list(map(lambda val: val + "=", properties))
    try:
        opts, args = getopt.getopt(sys.argv[1:], '', properties + ["profile="])
    except getopt.GetoptError as ex:
        print(ex)
        sys.exit(2)

    configuration = {}

    load_yaml_file('application.yaml', lambda val: merge_configuration(configuration, val))

    profile = get_profile(opts)

    if profile:
        load_yaml_file(f'application-{profile}.yaml', lambda val: merge_configuration(configuration, val))

    for opt, value in opts:
        configuration_value(configuration, opt[2:], value)

    return configuration
