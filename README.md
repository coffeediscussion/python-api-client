# Python RBC One Data API Client

## Introduction

This is the home for the Python 3.6 Demo Client code for RBC One Data APIs. The demo teaches you how to implement a client 
application service for RBC One Data API Version 2 in Python.
> The code in the repository is for demo purposes only.

## Prerequisites

The code in the repository requires Python 3.6 or higher. You will need to set up your machine to run Python 3.6.
You can find the installation instructions on the [Python - Setup and Usage](https://docs.python.org/3/using/index.html#python-setup-and-usage) page. You can 
run this application on Windows, Linux, macOS or in a Docker container. You'll need to install your favorite code 
editor. You can use whatever tools you are comfortable with.

We assume that you are familiar with the basics of Python Language and are able to [manage dependencies via virtual environments](https://docs.python.org/3/tutorial/venv.html); and understand the JSON format.

## Dependencies

The code has the following dependencies:

- Python 3.6.4 or higher

## Getting Started

### Sign up for RBC One Data API

Before you are able to use your client in production you must sign up with RBC for RBC One Data API. During the process of
signing up you will receive client-specific application ID (aka Client ID) and secret (aka Client Secret). These credentials
are required tor accessing the production data. 

Even though you will be able to develop and test your applications and services with the demo credentials, signing for the 
Data API gives you an access to additional development resources and technical support.

### Environments

The Data REST API is available for clients in two environments: QA and PROD. The QA environment should be
used for test and demo purposes. The PROD environment URL is provided by the RBC team upon signing up for the service.

### Authentication and Authorization

Data Security and Privacy at RBC is our highest priority. As an RBC client, you will benefit from a Data API architecture
built to meet the requirements of financial organizations.

As defined in [RFC 7519](https://www.rfc-editor.org/info/rfc7519), a [JSON Web Token (JWT)](https://en.wikipedia.org/wiki/JSON_Web_Token) 
is a safe way to represent a set of information between two parties - in our case between the server and the client.

In authentication, when the client successfully logs in using their credentials, a JSON Web Token will be returned and 
must be saved locally.

Whenever the client wants to access data, the client should send the JWT, in the 
[Authorization header](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Authorization) using the Bearer 
schema. This is a stateless authentication mechanism as the user state is never saved in server memory. The server's 
protected resources will check for a valid JWT in the Authorization header, and if it is present, the client will be 
allowed to access protected resources. As JWTs are self-contained, all the necessary information is there, reducing 
the need to query the database multiple times.

### Data API Client Implementation

An important point to mention is that the code in this repository is *NOT* a reference implementation.
It is just *one of many* possible implementations of the interaction between the client and the Data REST API server. 
The demo application only highlights the key points that any implementation of a Data API client must have. 

The first step in our application is to get authorized by the server. We send a POST request to the authorization 
endpoint with the credentials to receive a JSON Web Token. In this application, we send an authentication request
for each instance of the client's services. For production version, it makes sense to create a separate 
Authentication and Authorization service that will be responsible for managing and sharing the JWT across all other 
services.

After the JWT has been received, we use HTTP GET requests to retrieve all the information we need. 

We use the JSON Serializer to convert JSON data into Python Objects. Our first task is to define Python classes to 
contain the information we need from these responses. 

Then we create the base service with utility methods to send HTTP GET requests to the Data API server.

And finally we create a domain-specific service (such as FundAccountingService) to implement business specific logic.

You can find detailed information about the model and supported endpoints on the Swagger page in the RBC One Web Portal.

## Best Practices

### Limiting Amount of Transferred Data

All Fund Accounting service methods that return a list of items support pagination. Even though the page and size 
parameters are optional on the Data REST API side we highly encourage you to make them mandatory on the client side. 
Considering the high volume of data, not specifying the parameters may cause interruptions and service quality 
degradation during the data transfer between the servers and clients.

### Required Attributes Only

The Data REST API provides an extensive data model. You don't need to replicate all the provided attributes
in your code. It is a good practice to model only attributes you need and omit everything else. It will decrease the
development and maintenance cost.