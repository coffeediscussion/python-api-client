import json

from services.fund_accounting_service import FundAccountingService


def run(service: FundAccountingService):
    portfolios = service.list_portfolios(size = 5, start_date = "20190111", exclude = ("navDate", "changePercentTNav"))
    if not (type(portfolios) is list):
        print(f"Invalid response: {portfolios}")
    print(f"Number of portfolios returned: {len(portfolios)}")
    portfolio_id = portfolios[0]["_id"]

    details = json.dumps(portfolios[0], indent = 2)
    print(f"Portfolio #1 details:\n{details}")

    portfolio = service.find_portfolio(portfolio_id)
    details = json.dumps(portfolio, indent = 2)
    print(f"Portfolio {portfolio_id} details:\n{details}")

    print(f"Portfolio {portfolio_id} contains:")
    print(f"\t{len(service.list_balance_sheets(portfolio_id))} balance sheets")
    print(f"\t{len(service.list_positions(portfolio_id))} positions")
    print(f"\t{len(service.list_profit_and_losses(portfolio_id))} pnls")
    print(f"\t{len(service.list_share_classes(portfolio_id))} share classes")
    print(f"\t{len(service.list_transactions(portfolio_id))} transactions")
