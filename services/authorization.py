import json
from typing import Dict

import requests

from config.configuration import AuthEndpointConfiguration
from utils.url_utils import build_url


class Token(object):

    def __init__(self, access_token: str, token_type: str = 'Bearer', expires_in: int = 7199):
        self.expiresIn = expires_in
        self.tokenType = token_type
        self.accessToken = access_token


class Authorization(object):

    def __init__(self, config: Dict[str, str]):
        if config is None:
            config = {}
        self._config = AuthEndpointConfiguration(**config)

    def authorize(self):

        headers = {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Accept': 'application/json'
        }

        body = {
            "client_id": self._config.clientId,
            "client_secret": self._config.clientSecret,
            "scope": "read",
            "grant_type": "client_credentials"
        }

        try:
            url = build_url(self._config.host, (self._config.rootPath,), schema = self._config.scheme)
            return self.build_token(requests.post(url, data = body, headers = headers))
        except Exception as e:
            print('authorize() failed : ' + str(e))
            raise

    @staticmethod
    def build_token(response):
        if response.status_code != 200:
            raise Exception(f'Response status: {response.status_code}; {response.reason}')
        content_type = response.headers.get('Content-Type', 'text/plain')
        if content_type.startswith('text/plain'):
            values = response.text
            parts = values.split(' ')
            if len(parts) != 2:
                raise Exception(f'Invalid token format: {values}')
            return Token(access_token = parts[1])
        elif content_type.startswith('application/json'):
            return Token(**json.loads(response.text))
        else:
            raise Exception(f'Unsupported media type: {content_type}')
