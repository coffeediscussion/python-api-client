import logging

from dependency_injector import containers, providers

import runner
from services.authorization import Authorization
from services.fund_accounting_service import FundAccountingService


class IocContainer(containers.DeclarativeContainer):
    """Application IoC container."""

    config = providers.Configuration('config')
    logger = providers.Singleton(logging.Logger, name = 'python-api-client')

    auth_service = providers.Singleton(Authorization, config = config.rbcone.auth)

    fund_accounting_service = providers.Singleton(FundAccountingService,
                                                  authorization = auth_service,
                                                  config = config.rbcone.api)

    main = providers.Callable(runner.run, service = fund_accounting_service)
