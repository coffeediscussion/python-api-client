from typing import Dict, Tuple

import requests

from config.configuration import ApiEndpointConfiguration
from services.authorization import Authorization
from utils.url_utils import build_url


class BaseService(object):
    """ The class represents one of the approaches in implementation of the interaction with the Data REST API.
    One of the key points here is the authentication (and authorization) process. Before any requests to the Data
    REST API the client application (service) must receive JWT token from the authentication/authorization service.
    This token must be provided with each and every request to the rest of the services (i.e. Fund Accounting service).
    The authentication process requires Client ID (aka App ID, Application ID) and Client Secret. The credentials
    are provided by RBC.

    The Data REST API are available for the clients in two environments: QA and PROD. The QA environment should be
    used for test and demo purposes. The PROD environment URL is provided by RBC for each client separately.

    Attributes:
        _token:         JWT token received from the authorization service.
    """

    def __init__(self, config: Dict[str, str], authorization: Authorization):
        """In this implementation we perform authorization on service creation. Therefore each instance of the service
        will have its own JWT token. This is OK for demo and test purposes. However for production it would be better
        to implement a separate authorization service that will be responsible to retrieval and renewal of the JWT token
        and sharing it across all other services.
        """
        if config is None:
            config = {}
        self._config = ApiEndpointConfiguration(**config)
        self._token = authorization.authorize()

    @property
    def token(self) -> str:
        return f'{self._token.tokenType} {self._token.accessToken}'

    def get_request(self, path: Tuple[str, ...] = (), query: Dict[str, object] = None):
        """A utility method to send a GET request to the Data REST API. It builds an endpoint based on the RootPath,
        service path and query string. It also adds default headers to the request, including the acceptable
        media type and authorization header value (which is the JWT token received earlier).</para>

        Remarks:
            Please note that the current demo accepts JSON only. However the Data REST API services may provide data
            in other formats as well. Please refer to the Swagger web page for more details.

        Attributes:
            path:   A service/endpoint path. The value will be concatenated with the RootPath defined above.
            query:  An optional query string.

        Returns:
            A JSON object.
        """

        if query is None:
            query = {}

        headers = {
            "Accept": "application/json",
            "Authorization": self.token
        }

        try:
            parts: Tuple[str, ...] = (self._config.rootPath,) + path

            url = build_url(self._config.host, parts, query = query,
                            schema = self._config.scheme)
            print(url)
            response = requests.get(url, headers = headers)
            if response.status_code != 200:
                raise Exception(f'Response status: {response.status_code}; {response.reason}\n{response.content}')
            return response.json()

        except Exception as e:
            print('get_request() failed : ' + str(e))
