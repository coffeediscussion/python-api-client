from unittest import TestCase
from unittest.mock import Mock

from requests import Response

from services.authorization import Authorization


class TestAuthorization(TestCase):

    def test_authorize_json(self):
        response = Mock(spec = Response)
        response.headers = {'Content-Type': 'application/json'}
        response.text = '{"access_token' \
                        '":"eyJhbGciOiJSUzI1NiIsImtpZCI6InByb2RjZXJ0aWZpY2F0ZWtleSJ9' \
                        '","expires_in":7199,"token_type":"Bearer"} '

        token = Authorization.build_token(response)

        self.assertEqual('eyJhbGciOiJSUzI1NiIsImtpZCI6InByb2RjZXJ0aWZpY2F0ZWtleSJ9', token.accessToken)
        self.assertEqual('Bearer', token.tokenType)
        self.assertEqual(7199, token.expiresIn)

    def test_authorize_text(self):
        response = Mock(spec = Response)
        response.headers = {'Content-Type': 'text/plain'}
        response.text = 'Bearer ' \
                        'eyJhbGciOiJSUzI1NiIsImtpZCI6InByb2RjZXJ0aWZpY2F0ZWtleSJ9'

        token = Authorization.build_token(response)

        self.assertEqual('eyJhbGciOiJSUzI1NiIsImtpZCI6InByb2RjZXJ0aWZpY2F0ZWtleSJ9', token.accessToken)
        self.assertEqual('Bearer', token.tokenType)
        self.assertEqual(7199, token.expiresIn)
