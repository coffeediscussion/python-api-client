from typing import Tuple

from services.base_service import BaseService


class FundAccountingService(BaseService):
    """The class is a sample of domain-specific service implementation. This class represents Fund Accounting service.
     Please note that the class is for demo-purposes only and may be incomplete in terms of business functionality.
     The purpose of the class is to demo how the client's application/service can interact with the Data REST API.
     For full list of supported endpoints please refer to the Swagger web page.

     All Fund Accounting service methods that return a list of items support pagination and date range.

     Even though the page and size parameters are optional on the Data REST API side we highly encourage you to
     make them mandatory on the client side. Considering the high volume of data, not specifying the parameters
     may cause interruptions and service quality degradation during the data transfer between the servers and
     clients.
    """

    _endpoint: Tuple[str, ...] = ("fa", "v2", "portfolios")

    def find_portfolio(self, portfolio_id: str):
        return super().get_request(self._endpoint + (portfolio_id,))

    def list_entity_by_id(self, portfolio_id: str, entity: str, exclude: Tuple[str, ...] = None):
        return super().get_request(self._endpoint + (portfolio_id, entity),
                                   query = {
                                       "exclude": ",".join(exclude)
                                   } if not (exclude is None) else {})

    def list_balance_sheets(self, portfolio_id: str, exclude: Tuple[str, ...] = None):
        return self.list_entity_by_id(portfolio_id, "balancesheets", exclude)

    def list_portfolios(self, page: int = 0, size: int = 25, exclude: Tuple[str, ...] = None,
                        start_date: str = None, end_date: str = None):
        query = {
            "page": page,
            "size": size,
            "exclude": ",".join(exclude) if not (exclude is None) else None,
            "startDate": start_date,
            "endDate": end_date
        }
        return super().get_request(self._endpoint,
                                   query = {key: value for key, value in query.items() if not (value is None)})

    def list_positions(self, portfolio_id: str, exclude: Tuple[str, ...] = None):
        return self.list_entity_by_id(portfolio_id, "positions", exclude)

    def list_profit_and_losses(self, portfolio_id: str, exclude: Tuple[str, ...] = None):
        return self.list_entity_by_id(portfolio_id, "profitandlosses", exclude)

    def list_transactions(self, portfolio_id: str, exclude: Tuple[str, ...] = None):
        return self.list_entity_by_id(portfolio_id, "transactions", exclude)

    def list_share_classes(self, portfolio_id: str, exclude: Tuple[str, ...] = None):
        return self.list_entity_by_id(portfolio_id, "shareclasses", exclude)
