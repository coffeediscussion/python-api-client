import logging
import sys

from config.configuration_utils import load_configuration
from services.container import IocContainer

if __name__ == "__main__":

    container = IocContainer(config = load_configuration(["rbcone.auth.client_id", "rbcone.auth.client_secret"]))

    container.logger().addHandler(logging.StreamHandler(sys.stdout))

    container.main()
